CREATE DATABASE V_K_module3
GO
use V_K_module3
GO


CREATE TABLE students (
id INT NOT NULL, --PRIMARY KEY, --identity adduser id int identity (1.1)
student_id INT not null PRIMARY KEY,
ID_Group int null,
first_name VARCHAR(25),
last_name VARCHAR (25),
surname VARCHAR(25),
date_birth DATE NOT NULL,
id_card_number INT NOT NULL UNIQUE,
city VARCHAR (20),
home_adress VARCHAR (50),
mobile_phone VARCHAR (12),
adress VARCHAR(50),
inserted_date DATE NOT NULL,
updated_date DATE NOT NULL 

Constraint FK_2 FOREIGN KEY (ID_Group)
REFERENCES dbo.info (ID_Group)
ON DELETE Cascade
on update set null)

alter table dbo.addiona
add  student_id INT not null


use V_K_module3
alter table dbo.student
add constraint inserted_date DEFAULT (getdate())
FOR inserted_date
GO

ALTER TABLE dbo.student
ADD CONSTRAINT id_card_number UNIQUE (id_card_number)
go

alter table dbo.student
add constraint FK_1
FOREIGN KEY ( student_id)
REFERENCES additional_info (student_id)
go
update V_K_module3.dbo.student
SET inserted_


update V_K_module3.dbo.student
SET updated_date = GETDATE ()